import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @author legkunec
 * @since 14.05.14 11:01
 */
public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        connect();
        Thread.sleep(10000);
    }

    private static void receive(final AsynchronousSocketChannel channel) throws IOException {
            final ByteBuffer in = ByteBuffer.allocate(4);
                channel.read(in, 500, TimeUnit.MILLISECONDS, in, new CompletionHandler<Integer, ByteBuffer>() {
                    @Override
                    public void completed(Integer result, ByteBuffer attachment) {
                        System.out.println("Readed: " + result + "in.array() = " + Arrays.toString(attachment.array()));
                        try {
                            receive(channel);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failed(Throwable exc, ByteBuffer attachment) {
                        exc.printStackTrace();
                    }
                });
    }

    private static void send(AsynchronousSocketChannel channel) throws IOException {
        final Charset charset = Charset.defaultCharset();
        final ByteBuffer out = charset.encode("Ping");
        channel.write(out);
        System.out.println("Sending ping");
    }

    private static AsynchronousSocketChannel connect() throws IOException {
        final AsynchronousSocketChannel channel = AsynchronousSocketChannel.open();
//        channel.socket().setSoTimeout(100);
//        channel.configureBlocking(false);
        final long startTime = System.nanoTime();
        try {
            channel.connect(new InetSocketAddress("192.168.3.174", 12345), null, new CompletionHandler<Void, Object>() {
                @Override
                public void completed(Void result, Object attachment) {
                    try {
                        send(channel);
                        receive(channel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failed(Throwable exc, Object attachment) {
                    exc.printStackTrace();
                }
            });
        } finally {
            final long endTime = System.nanoTime();
            System.out.println("connection time = " + (endTime - startTime) / (1000 * 1000));
//            System.out.println("connected = " + channel.isConnected());
        }
        return channel;
    }
}
