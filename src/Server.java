import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * @author legkunec
 * @since 14.05.14 11:00
 */
public class Server {
    public static void main(String[] args) throws IOException, InterruptedException {
        listen();
    }

    private static void listen() throws IOException, InterruptedException {
        final ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(true);
        serverSocketChannel.bind(new InetSocketAddress(12345));
        while (true){
            final SocketChannel channel = serverSocketChannel.accept();
            receive(channel);
            send(channel);
            channel.finishConnect();
        }
    }

    private static void receive(SocketChannel channel) throws IOException {
        final ByteBuffer in = ByteBuffer.allocate(4);
        channel.read(in);
        System.out.println("in.array() = " + Arrays.toString(in.array()));
    }

    private static void send(SocketChannel channel) throws IOException, InterruptedException {
        final Charset charset = Charset.defaultCharset();
        while (true) {
            final ByteBuffer out = charset.encode("Pong");
            channel.write(out);
            System.out.println("Responding pong");
            out.clear();
            Thread.sleep(1000);
        }
    }
}
