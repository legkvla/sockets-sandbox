require 'socket'

socket = nil
while socket.nil?
  # на каждой итерации пытаемся получить экземпляр соединения (сокета), если отсутствует
  start_time = Time.now
  begin
    host = '127.0.0.1'
    port = 12345

    addr = Socket.getaddrinfo(host, nil)
    socket = Socket.new(Socket.const_get(addr[0][0]), Socket::SOCK_STREAM, 0)
    socket.setsockopt(Socket::SOL_SOCKET, Socket::SO_KEEPALIVE, true)
    socket.connect(Socket.pack_sockaddr_in(port, addr[0][3]))
  rescue => err
    puts "Error: #{err.inspect}"
    sleep rand 0
    next
  ensure
    puts "Connection time: #{Time.now - start_time}"
  end
  puts 'Sending ping'
  socket.print "Ping"
  #socket.print "GET / HTTP/1.0\r\n\r\n"
  response = nil
  begin
    start_time = Time.now
    begin
      response = socket.read(4)
    rescue => err
      puts "Error: #{err.inspect}"
      socket.close
      sleep rand 0
      break
    ensure
      puts "Reading time: #{Time.now - start_time}"
    end
    puts "Receiving response: #{response}"

  end while response
end