require 'socket'

server = TCPServer.open(12345)
server.setsockopt(Socket::SOL_SOCKET,Socket::SO_KEEPALIVE, true)
loop {
  Thread.start(server.accept) do |socket|
    begin
      request = socket.read(4)
      puts "Request: #{request}"
      loop {
        socket.print "Pong"
        sleep 1
        puts "Response sent: Pong"
      }
    rescue Exception => err
      puts "Error: #{err.inspect}"
      # todo: disconnect and reinit session
    ensure
      socket.close
    end
  end
}
